.PHONY: all clean clean-all get-deps check-env

ICONS = $(wildcard icons/*.svg)

all: check-env get-deps targro

targro: main.go icons.go
	(GIT_COMMIT=$(shell git rev-list --abbrev-commit -1 HEAD); go build -i -ldflags "-X main.VersionCommit=$${GIT_COMMIT}" -o targro *.go )

icons.go: ${GOPATH}/bin/go-bindata ${ICONS}
	go-bindata -pkg main -o icons.go ${ICONS}

${GOPATH}/bin/go-bindata:
	go get -u github.com/go-bindata/go-bindata/...

check-env:
ifndef GOPATH
	$(error GOPATH is undefined, please configure your env for building Golang using GOPATH)
	# or update this file to be compatible with any other way
endif

get-deps:
	go get -d -v ./

clean:
	rm -f targro

clean-all: clean
	rm -f icons.go
