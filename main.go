// go build . && /usr/bin/time -v ./targro $HOME/Downloads /tmp/tg-cache
//
// or for devel (gitlab.com/queria/scripts has runonchange):
// runonchange ./main.go 'pkill targro; go build . && ( ./targro -v ~/Downloads /tmp/tg-cache & )'
package main

/******
* Copyright © 2019, Queria Sa-Tas <public@sa-tas.net> All rights reserved.
*
* Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
*
*   * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
*
*   * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
*
*   * Neither the name of the Queria Sa-Tas nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL QUERIA SA-TAS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING INCLUDING ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

import (
    "bytes"
    "flag"
    "fmt"
    "crypto/sha1"
    "encoding/base64"
    "errors"
    "net/http"
    "os/exec"
    "io"
    "io/ioutil"
    "mime"
    "path/filepath"
    "os"
    "regexp"
    "runtime"
    "sort"
    // "os/exec"
    // "strconv"
    "strings"
    // "sort"
    "time"

    "bitbucket.org/zombiezen/cardcpx/natsort"
)


var (
    arg_help bool = false
    arg_listen string = ":8811"
    arg_safe_gz bool = false
    arg_download_whole_pattern = ".*/([^/]+[/-][0-9]+)/$"
    arg_verbose bool = false
)

var AuthorRepoURL string = "https://gitlab.com/queria/targro/"
var AuthorCommitURL string = "https://gitlab.com/queria/targro/-/commit/"  // keep it pointing to url where specific commit can be appended to
var Version string = "0.2"
var VersionCommit string // value injected at build time via ldflags-X (see Makefile)
var TAR_SUFFIX = ".tar.gz"
var DECOMP_SUFFIX = "__decomp" // appended to name of the original tar.gz to get folder holding its content
var DOWNLOAD_WHOLE_PATTERN *regexp.Regexp // pattern for url path matching, initialized at startup

func _err(e error) {
    if e != nil {
        _, file, line, ok := runtime.Caller(1)
        if (ok) {
            fmt.Printf("ERROR: %s in %s#%d\n", e, file, line)
        } else {
            fmt.Println("ERROR: ", e)
        }
        os.Exit(1)
    }
}

func debug(msgs ...interface{}) {
    if arg_verbose {
        fmt.Fprintf(os.Stderr, "[debug] %v\n", msgs)
    }
}

func timeit(label string, started time.Time) {
    if arg_verbose {
        fmt.Fprintln(os.Stderr, "[timeit]", time.Since(started), label)
    }
}

func timeitSlow(request *http.Request, label string, started time.Time) {
    diff := time.Since(started)
    if(diff.Milliseconds() > 250) {
        addr := request.Header.Get("X-Real-IP")
        if(len(addr) == 0) {
            addr = request.Header.Get("X-Forwarded-For")
        }
        if(len(addr) == 0) {
            addr = request.RemoteAddr
        } else {
            addr += " via " + request.RemoteAddr
        }
        fmt.Fprintf(os.Stderr, "[slow] %s in %s for %s: %s\n", diff, label, addr, request.URL)
    }
}

func readlink_f(path string) string {
    path, err := filepath.Abs(path)
    _err(err)

    _, err = os.Stat(path)
    if(err == nil) { // translate symlinks only if path exists
        path, err = filepath.EvalSymlinks(path)
        _err(err)
    }
    return path
}

func dirExists(path string) bool {
    info, err := os.Stat(path)
    return (err == nil && info.IsDir())
}

func IndividualGzippedContent(filePath string) string {
    if(!strings.HasSuffix(filePath, ".gz") || strings.HasSuffix(filePath, TAR_SUFFIX)) {
        return "";
    }
    if (arg_safe_gz) {
        gz_dot_pos := strings.LastIndex(filePath, ".") // we already know this one is there since above test
        dot_pos := strings.LastIndex(filePath[:gz_dot_pos], ".")
        if (dot_pos < 0) { return ""; } // no dot in name
        return mime.TypeByExtension(filePath[dot_pos:gz_dot_pos])
    }
    return "text/plain"
}

//type PathSize struct {
//    path string
//    size int64
//}
//
//func totalSize_Walk(path string) (int64, []PathSize) {
//    var size int64 = int64(0)
//    var files []PathSize
//    err := filepath.Walk(path, func(path string, info os.FileInfo, err error) error {
//        if(err != nil) {
//            fmt.Println(err)
//            return nil
//        }
//        if (arg_summary_only) {
//            size += info.Size()
//            return nil
//        }
//        ps := PathSize{path, info.Size()}
//        size += ps.size
//        files = append(files, ps)
//        return nil
//    })
//    _err(err)
//    return size, files
//}
//
//func totalSize_IOReadDir(path string) int64 {
//    var size int64 = int64(0)
//    var dirs []string
//    var dir string
//
//    dirs = append(dirs, path)
//    for (len(dirs) > 0) {
//        dir, dirs = dirs[0], dirs[1:]
//        files, err := ioutil.ReadDir(dir)
//        _err(err)
//        for _, f := range files {
//            // fmt.Println(f.Name(), " ", f.IsDir())
//            if (f.IsDir()) {
//                dirs = append(dirs, filepath.Join(dir, f.Name()))
//            }
//            size += f.Size()
//        }
//    }
//
//    return size
//}

func cmd2str(cmd *exec.Cmd) string {
    dir := cmd.Dir
    if (len(dir) > 0) { dir = "cd "+dir+";" }
    return (
        dir + " " +
        strings.Join(cmd.Env, " ") +
        cmd.Path + " " +
        strings.Join(cmd.Args[1:], " "))
}




type SelfDeterminingHandler interface {
    SupportsPath(string) bool
    ServeHTTP(http.ResponseWriter, *http.Request)
}

type TargroServeMux struct {
    FileServer http.Handler
    ArchiveHandler SelfDeterminingHandler
}

func (m *TargroServeMux) Handler(request *http.Request) http.Handler {
    // defer timeit("routeRequest: " + request.URL.Path + " for " + request.RemoteAddr, time.Now())
    if (m.ArchiveHandler.SupportsPath(request.URL.Path)) {
        return m.ArchiveHandler
    }
    return m.FileServer
}

func (m TargroServeMux) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
    m.Handler(request).ServeHTTP(writer, request)
}



/*
type AssetHandler struct {
    Prefix string
}

func (h *AssetHandler) SupportsPath(path string) bool {
    return strings.HasPrefix(path, h.Prefix)
}

func (h *AssetHandler) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
    assetPath := request.URL.Path[ len(h.Prefix) : ]
    info, err := AssetInfo(assetPath)
    if(HandleFileErrors(writer, request, err, assetPath)) { return }
    if(!info.IsDir()) {
        http.ServeContent(writer, request, info.Name(), info.ModTime(), 
        HERE WOULD NEED io.ReadSeeker from []byte
    }

}
*/


type TargroHandler struct {
    SourceDir string
    CacheDir string

    fileServer http.Handler
    decompSuffix string // likely replace this suffixing with some cheap hashing?
    tarSuffix string
}

func NewTargroHandler(source_dir string, cache_dir string, fileServer http.Handler) *TargroHandler {
    h := TargroHandler{}

    h.SourceDir = source_dir
    h.CacheDir = cache_dir

    h.fileServer = fileServer
    h.decompSuffix = "__decomp"
    h.tarSuffix = TAR_SUFFIX

    h.initCache()

    return &h
}

func (h *TargroHandler) SupportsPath(path string) bool {
    return strings.Contains(path, h.tarSuffix)
}

func (h *TargroHandler) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
    defer timeitSlow(request, "ServeHTTP+decompressSource", time.Now())
    source_parts := h.urlToParts(request.URL.Path)
    cached_path, err, stderr := h.decompressSource(source_parts[0])
    if (err != nil) {
        debug("interal error in decompressSource:", err, stderr)
        if (len(stderr)>0) {
            http.Error(writer, "HTTP 500 - Internal server error - decompression failed\n\n" + err.Error() + "\n" + stderr, http.StatusInternalServerError)
            return
        }
    }
    if (HandleFileErrors(writer, request, err, source_parts[0])) {
        return
    }
    if ("" == source_parts[1] && '/' != request.URL.Path[len(request.URL.Path) - 1]) {
        fmt.Println("Path is", request.URL.Path)
        http.Redirect(writer, request, request.URL.Path + "/", http.StatusMovedPermanently)
        return
    }
    // FIXME: replace ServeFile with ServeContent to avoid auto-magic of index.html handling
    ServeFile(writer, request, cached_path + source_parts[1], h.CacheDir)
}

func (h *TargroHandler) urlToParts(urlPath string) []string {
    // this is internal func, as it's to be replaced fully by regexp matching
    // there will be need for it at all afterwards
    // as also tarSuffix will be gone, likely replaced by "static" regexp
    parts := strings.SplitN(urlPath, h.tarSuffix, 2)
    parts[0] = parts[0] + h.tarSuffix
    return parts
}

func (h *TargroHandler) decompressSource(source_path string) (string, error, string) {
    decomp_path := filepath.Join(h.CacheDir, "decomp", source_path + h.decompSuffix)
    var err error
    if(!dirExists(decomp_path)) {
        full_source_path := h.SourceDir + source_path
        defer timeit("decompressSource: " + full_source_path, time.Now())
        _, err = os.Stat(full_source_path)
        if (err != nil) {
            return "", err, ""
        }
        err = os.MkdirAll(decomp_path, 0755)
        if (err != nil) {
            return "", err, ""
        }

        untar := exec.Command("tar", "xf", full_source_path)
        untar.Dir = decomp_path
        var stderr bytes.Buffer
        untar.Stderr = &stderr

        fmt.Println(cmd2str(untar)) // debug only? stderr?
        err = untar.Run()
        if (err != nil) {
            os.RemoveAll(decomp_path)
            return "", err, stderr.String()
        }
    }
    return decomp_path, nil, ""
}

func (h *TargroHandler) initCache() {
    defer timeit("initCache", time.Now())
    cd_info, err := os.Stat(h.CacheDir)
    if (err == nil && !cd_info.IsDir()) {
        fmt.Println("ERROR: cache dir " + h.CacheDir + " is not a directory!")
        os.Exit(1)
    } else if (err != nil && os.IsNotExist(err)) {
        err = os.MkdirAll(h.CacheDir, 0755)
        // this dir will be also (re)created due to MkdirAll usage when
        // decompressing any/each requested archive,
        // but we want to attempt creating it on startup
        // to fail early if there is e.g. permission issues
    } else {
        // cache exists and is a dir
        // TODO: scan CacheDir contents and populate runtime list of existing content for cleanups
    }
    if(err == nil) {
        err = os.MkdirAll(filepath.Join(h.CacheDir, "decomp"), 0755)
        if(err != nil && os.IsExist(err)) { err = nil }
        if(err == nil) {
            err = os.MkdirAll(filepath.Join(h.CacheDir, "comp"), 0755)
            if (err != nil && os.IsExist(err)) { err = nil }
        }
    }
    _err(err)
}



type FileServer struct {
    rootPath string
    CacheDir string
}

func NewFileServer(source_dir string, cache_dir string) (*FileServer, error) {
    info, err := os.Stat(source_dir)
    if (err != nil) { return nil, err; }
    if (! info.IsDir()) {
        return nil, errors.New(fmt.Sprintf("Source dir %s is not a directory!", source_dir))
    }
    fs := FileServer{source_dir, cache_dir}
    return &fs, nil
}

func (fs FileServer) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
    if (request.URL.Path == "/favicon.ico") {
        // If we ever want favicon, likely handle it here (or maybe at Mux level if still there?)
        http.NotFound(writer, request)
        return
    }
    rpath := fs.rootPath + request.URL.Path
    ServeFile(writer, request, rpath, fs.CacheDir)
}

func DirListing(writer http.ResponseWriter, request *http.Request, dirPath string) {
    // generates html page which lists all entries in directory dirPath
    defer timeitSlow(request, "DirListing", time.Now())
    dirsFirst := true

    path := request.URL.Path

    files, err := ioutil.ReadDir(dirPath)
    _err(err)

    sort_by := request.FormValue("C")
    sort_order := request.FormValue("O")
    if (sort_by != "M" && sort_by != "S") { sort_by = "N" }
    if (sort_order != "D") { sort_order = "A" }
    if (sort_by == "M") { dirsFirst = false }

    var dirsfun func(int, int, bool) bool
    if (dirsFirst) {
        dirsfun = func (i, j int, nameIsLess bool) bool {
            if (files[i].IsDir() == files[j].IsDir()) {
                return nameIsLess
            }
            return files[i].IsDir()
        }
    } else {
        dirsfun = func (i, j int, nameIsLess bool) bool { return nameIsLess; }
    }

    var sortfun func(int, int) bool
    switch (sort_by+sort_order) {
    case "MA": sortfun = func (i, j int) bool { return dirsfun(i, j, files[i].ModTime().Before(files[j].ModTime())); }
    case "MD": sortfun = func (i, j int) bool { return dirsfun(i, j, files[i].ModTime().After(files[j].ModTime())); }
    case "SA": sortfun = func (i, j int) bool { return dirsfun(i, j, (files[i].Size() < files[j].Size())); }
    case "SD": sortfun = func (i, j int) bool { return dirsfun(i, j, (files[i].Size() > files[j].Size())); }
    case "ND": sortfun = func (i, j int) bool { return dirsfun(i, j, natsort.Less(strings.ToLower(files[j].Name()), strings.ToLower(files[i].Name()))); } // Name, Ascending
    default: sortfun = func (i, j int) bool { return dirsfun(i, j, natsort.Less(strings.ToLower(files[i].Name()), strings.ToLower(files[j].Name()))); } // Name, Ascending
    }
    sort.SliceStable(files, sortfun)

    // FIXME: move this out of this function, we should not encode it on every request
    //        also likely expose via AssetHandler to allow caching of the images?
    asset, _ := Asset("icons/folder.svg")
    ico64_folder := "data:image/svg+xml;base64," + base64.StdEncoding.EncodeToString(asset)
    asset, _ = Asset("icons/file.svg")
    ico64_file := "data:image/svg+xml;base64," + base64.StdEncoding.EncodeToString(asset)
    asset, _ = Asset("icons/corner-left-up.svg")
    ico64_leftUp := "data:image/svg+xml;base64," + base64.StdEncoding.EncodeToString(asset)


    page := "<html><head><title>Index of " + path + "</title>"
    page += "<style>"
    page += " a {color:#229;text-decoration:none;} a:hover {color:#00F;text-decoration:underline;} a:visited {color:#606;}"
    page += " .download { font-style: italic; padding-bottom: 1em; }"
    page += " tr.entry:hover { background-color: #ffd; }"
    page += " td { padding:0em 1em 0em 1em; }"
    // note:  top-padding in name collumn + min-height of the block A tag inside it should sum up to match icon sizes
    //        so if tweaking these, keep them in some relative formula like:    .name[background-size] == .name[padding-top] + .name>a[min-height]
    page += " .name { background-repeat: no-repeat; background-size: 2em 2em; background-image: url(" + ico64_file +"); padding:0.3em 0em 0em 0em; }"
    page += " .name a { display:inline-block; padding-left: 2.5em; min-height: 1.7em; }"
    page += " .up { background-image: url(" + ico64_leftUp +"); }"
    page += " .dir { background-image: url(" + ico64_folder +"); }"
    page += " .file {  }"
    page += " .size { text-align:right; padding-left:2em; }"
    page += " #footer { color:#999; float:right; } #footer a { color: #999; }"
    page += "</style>"
    page += "</head>"
    page += "<body>"
    // TODO: make all elements of path here hyperlinks - individually, leading directly to folders up in hierarchy
    //    example =>   <h1>Index of /<a href="../../">somejob</a>/<a href="../">buildNum</a>/</h1>
    //    likely do some split by '/', count parts, then iterate and add (sum-counter)*'../' as hyperlink, then join together?
    //    or more likely print parts in loop, we do not need them actually joined as they are appended to 'page'
    page += "<h1>Index of " + path + "</h1>"
    if(DOWNLOAD_WHOLE_PATTERN != nil) {
        if(len(downloadWholeArchiveName(path)) > 0) {
            page += "<div class=\"download\"><a href=\"?download\">Download whole directory in archive <u>&#x21e9;</u></a></div>"
        }
    }
    page += "<table style=\"font-family: monospace;\">"
    colHead := func (name string, sortChar string) string {
        order := "A"
        orderSign := ""
        if (sortChar == sort_by) {
            if (sort_order == "A") {
                order = "D"; orderSign = " &darr;"
            } else {
                orderSign = " &uarr;"
            }
        }
        col := fmt.Sprintf(
            "<th><a href=\"?C=%s&O=%s\">%s%s</a></th>",
            sortChar, order, name, orderSign)
        return col
    }
    page += "<tr>" + colHead("Name", "N") + colHead("Last modified", "M") + colHead("Size", "S") + "</tr>"
    page += "<tr><td colspan=\"3\"><hr /></td></tr>"

    if (path != "" && path != "/") {
        page += "<tr><td class=\"name up\"><a href=\"../\">..</a></td><td></td><td></tr>"
    }

    var hasReadme bool = false
    afterLastDir := false
    for _, f := range files {
        fName := f.Name()
        css := "file"
        if (f.IsDir()) {
            fName += "/"
            css = "dir"
        } else {
            if(dirsFirst && !afterLastDir) {
                afterLastDir = true
                page += "<tr><td colspan=\"3\"><hr /></td></tr>"
            }
            if (!hasReadme && f.Name() == "README.html") {
                hasReadme = true
            }
        }
        // TODO: for supported archives (TAR_SUFFIX or individual .gz) append [&darr;] hyperlink for downloading as whole file
        page += fmt.Sprintf(
            "<tr class=\"entry\"><td class=\"name %s\"><a href=\"./%s\">%s</a></td><td>%s</td><td class=\"size\">%d</td></tr>",
            css, fName, fName,
            f.ModTime().Format("02-Jan-2006 15:04"),
            f.Size())
    }
    page += "<tr><td colspan=\"3\"><hr /></td></tr>"
    page += "</table>"
    if (hasReadme) {
        readme, err := ioutil.ReadFile(filepath.Join(dirPath, "README.html"))
        if (err != nil) {
            fmt.Println("WARNING: README failed:", err)
            page += "<div>Failed to render README here: "+err.Error()+"</div>"
        } else {
            page += "<div class=\"readme\">" + string(readme) + "</div>"
        }
    }
    page += fmt.Sprintf(
        "<div id=\"footer\"><a href=\"%s\">targro</a> v%s#<a href=\"%s%s\">%s</a></div>",
        AuthorRepoURL, Version,
        AuthorCommitURL, VersionCommit, VersionCommit)
    page += "</body>"
    page += "</html>"

    // writer.Header().Set("ETag", etagHash(page))

    io.WriteString(writer, page)
}

func HandleFileErrors(writer http.ResponseWriter, request *http.Request, err error, filePath string) bool {
    if (err != nil) {
        switch {
        case os.IsNotExist(err):
            // 404
            fmt.Printf(
                "ERROR: FileNotFound accessing %s => %s for %s: %s\n",
                request.URL.Path,
                filePath,
                request.RemoteAddr,
                err)
            http.NotFound(writer, request)
            return true
        case os.IsPermission(err):
            // 403
            fmt.Printf(
                "ERROR: Forbidden issue when acccessing %s => %s for %s: %s\n",
                request.URL.Path,
                filePath,
                request.RemoteAddr,
                err)
            http.Error(writer, err.Error(), http.StatusForbidden)
            return true
        default:
            fmt.Printf(
                "ERROR: Unkown issue when acccessing %s => %s for %s: %s\n",
                request.URL.Path,
                filePath,
                request.RemoteAddr,
                err)
            http.Error(writer, err.Error(), http.StatusInternalServerError)
            return true
            // 500
        }
    }
    return false
}

func ServeFile(writer http.ResponseWriter, request *http.Request, filePath string, cacheDir string) {
    info, err := os.Stat(filePath)
    if(HandleFileErrors(writer, request, err, filePath)) { return }

    if (info.IsDir()) {
        if (request.URL.Path[len(request.URL.Path) - 1] != '/') {
            http.Redirect(writer, request, request.URL.Path + "/", http.StatusMovedPermanently)
            return
        }
        if(DOWNLOAD_WHOLE_PATTERN != nil) {
            request.ParseForm()
            _, downloadRequested := request.Form["download"]
            if (downloadRequested) {
                archiveName := downloadWholeArchiveName(request.URL.Path)
                if(len(archiveName) > 0) {
                    err := DirAsArchive(writer, request, filePath, archiveName, cacheDir)
                    if(err != nil) {
                        http.Error(
                            writer,
                            "HTTP 500 - Internal error: Failed to prepare the download\n" + err.Error(),
                            http.StatusInternalServerError)
                    }
                    return
                } else {
                    http.Error(writer, "Download of this directory in single archive is not allowed.", http.StatusForbidden)
                    return
                }
            }
        }
        DirListing(writer, request, filePath)
    } else {
        defer timeitSlow(request, "ServeFile/single", time.Now())
        file, err := os.Open(filePath)
        _err(err)
        defer file.Close()
        // writer.Header().Set("ETag", etagHash(
        //     fmt.Sprintf("%s %d", info.ModTime(), info.Size())))
        if (strings.Contains(request.Header.Get("Accept-Encoding"), "gzip")) {
            gzip_content := IndividualGzippedContent(filePath)
            if(gzip_content != "") {
                writer.Header().Set("Content-Encoding", "gzip")
                writer.Header().Set("Content-Type", gzip_content)
            }
        }
        http.ServeContent(writer, request, info.Name(), info.ModTime(), file)
    }

    // TODO: check if we do need to prevent '../../' abuse here?
    // get filepath-modified time (Stat)
    // if err =>  not-found || perm-denied || internal-error
    // if (ismodifiedsince >= filepath-modified) { writer << notmodified;  return; }
    // writer << modified headers
    // if (filepath is dir) {
    //   // TODO: do we need to expl. redirect '/dirA' => '/dirA/' ?
    //   writer << listDir(filepath)
    // } else {
    //   http.ServeContent(...,  io.File(filepath))
    // }
}

func DirAsArchive(writer http.ResponseWriter, request *http.Request, dirPath string, archiveBaseName string, cacheDir string) (error) {
    defer timeitSlow(request, "DirAsArchive", time.Now())
    // TODO: this should be protected via flock
    pathSum := fmt.Sprintf("%x", sha1.Sum([]byte(dirPath)))
    archivePath := filepath.Join(cacheDir, "comp", pathSum + ".tar.zst")
    archiveUserName := archiveBaseName + ".tar.zst"
    debug("This should be archived content of", dirPath, "stored in", archivePath, "as", archiveUserName)

    cleanupIncomplete := false
    info, err := os.Stat(archivePath)

    if (err != nil) {
        if (!os.IsNotExist(err)) {
            return err
        }
        cleanupIncomplete = true
        defer func() {
            if(cleanupIncomplete) {
                os.RemoveAll(archivePath)
            }
        }()

        var stderrTar bytes.Buffer
        var stderrComp bytes.Buffer

        base := filepath.Base(dirPath)
        tar := exec.Command("tar", "c", "--transform", "s|^"+base+"|"+archiveBaseName+"|", base)
        tar.Dir = filepath.Dir(filepath.Dir(dirPath)) // double since we already point into/inside-of requested dir
        tar.Stderr = &stderrTar

        compression := exec.Command("zstd", "--long", "--fast", "-", "-o", archivePath)
        compression.Stderr = &stderrComp

        compression.Stdin, err = tar.StdoutPipe()
        if(err != nil) { return err }

        debug(cmd2str(compression))
        debug(cmd2str(tar))

        // possibly we could move this into goroutine and start streaming the content to user?
        // or compression can output to stdout which we could read and write to both [response, dest file].
        err = compression.Start()
        if(err != nil) { return fmt.Errorf("%w\n%v", err, stderrComp.String()) }
        err = tar.Run()
        if(err != nil) { return fmt.Errorf("%w\n%v", err, stderrTar.String()) }
        err = compression.Wait()
        if(err != nil) { return fmt.Errorf("%w\n%v", err, stderrComp.String()) }

        cleanupIncomplete = false
        info, err = os.Stat(archivePath)
    }

    if (err != nil) {
        return err
    }

    file, err := os.Open(archivePath)
    if (err != nil) { return err }
    defer file.Close()

    writer.Header().Set("Content-Disposition", "attachment; filename=\"" + archiveUserName + "\"")
    http.ServeContent(writer, request, archiveUserName, info.ModTime(), file)
    return nil
}

func downloadWholeArchiveName(path string) (string) {
    name := ""
    match := DOWNLOAD_WHOLE_PATTERN.FindStringSubmatch(path)
    if(match != nil) {
        if(len(match) > 1 && match[1] != "") {
            parts := strings.Split(match[1], "/")
            name = strings.Trim(strings.Join(parts, "-"), "-")
        }
    }
    return name
}

func etagHash(input string) string {
    return fmt.Sprintf("%x", sha1.Sum([]byte(input)))
}



func main() {
    defer timeit("main", time.Now())
    fmt.Fprintf(os.Stderr, "targro v%s #%s\n", Version, VersionCommit)

    flag.BoolVar(&arg_verbose, "v", false, "Enable verbose/debug output")
    flag.BoolVar(&arg_safe_gz, "g", false, "Use safe .gz handling (send original guessed mime-type for individual files)")
    flag.BoolVar(&arg_help, "h", false, "Show help")
    flag.BoolVar(&arg_help, "help", false, "")
    flag.StringVar(&arg_listen, "l", arg_listen, "Which `address:port` should server listen on")
    flag.StringVar(
        &arg_download_whole_pattern, "d", arg_download_whole_pattern,
        "Regexp pattern specifying which directory paths will have download-as-single-archive enabled.\n" +
        "Single (1st) capture group is used to define archive name with slashes replaced by dashes.\n" +
        "Empty string will disable this whole feature.\n")
    flag.Parse()

    if (arg_help || flag.NArg() < 2) {
         fmt.Println("Usage:  ./targro [opts] <source_dir> <cache_dir>")
         fmt.Println("Options available:")
         flag.PrintDefaults()
         os.Exit(2)
    }
    source_dir := flag.Arg(0)
    cache_dir := flag.Arg(1)
    source_dir = readlink_f(source_dir)
    cache_dir = readlink_f(cache_dir)
    if (arg_download_whole_pattern != "") {
        DOWNLOAD_WHOLE_PATTERN = regexp.MustCompile(arg_download_whole_pattern)
    }
    fmt.Fprintln(os.Stderr, "Source path: ", source_dir)
    fmt.Fprintln(os.Stderr, "Cache path: ", cache_dir)
    fmt.Fprintln(os.Stderr, "Going to listen on:", arg_listen)
    fmt.Fprintln(os.Stderr, "Whole download pattern:", DOWNLOAD_WHOLE_PATTERN)

    /*
    tmp_time := time.Now()
    fmt.Println("IOReadDir:", totalSize_IOReadDir(start_dir), "bytes")
    timeit("IOReadDir", tmp_time)
    */

    // initCache(cache_dir)
    // fileServer := http.FileServer(http.Dir(source_dir))

    // fsh := http.FileServer(http.Dir(source_dir))
    fsh, err := NewFileServer(source_dir, cache_dir) // FIXME(psedlak-critical): we should not pass cache_dir there, instead extract decompress/compress out to separate type/helpers
    _err(err)
    tgh := NewTargroHandler(source_dir, cache_dir, fsh)

    tgmux := TargroServeMux{fsh, tgh}

    s := http.Server{
        Addr: arg_listen,
        Handler: tgmux}
    // http.HandleFunc("/", routeRequest)
    s.ListenAndServe()
    //tmp_time := time.Now()
    //_s, _f := totalSize_Walk(start_dir)
    //_f = _f
    //fmt.Println("Walk:", _s, "bytes in", len(_f), "files")
    //timeit("Walk", tmp_time)

    _ = io.WriteString // FIXME: remove later, io.Writestring used sometimes for debug, this is here to not have unused-import
}
